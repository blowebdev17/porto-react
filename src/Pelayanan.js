import React, { useState, useEffect } from "react";
import { useNavigate } from 'react-router-dom';
import Header from './Header';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';

function Pelayanan() {		
	return (
	 <>
		<Header />
		<br />
		<div className="container-fluid">
			<Card className="textCard">
		      <Card.Body>
		        <Card.Title>Pelayanan Kami</Card.Title>
		        <Card.Text>
		          Kami melayani apapun kebutuhan anda saat ini.
		        </Card.Text>
		      </Card.Body>
		      <ListGroup className="list-group-flush">
		        <ListGroup.Item> Pencetakan untuk keperluan Kantor (ATK)</ListGroup.Item>
		        <ListGroup.Item> Berbagai jenis Buku</ListGroup.Item>
		        <ListGroup.Item>Booklet</ListGroup.Item>
		        <ListGroup.Item>Brosur</ListGroup.Item>
		        <ListGroup.Item>Flyer</ListGroup.Item>
		        <ListGroup.Item>Aneka jenis Kartu</ListGroup.Item>
		        <ListGroup.Item>Signboard</ListGroup.Item>
		        <ListGroup.Item>Banner</ListGroup.Item><ListGroup.Item>Sticker dan sebagainya dengan berbagai ukuran dan jenis bahan kertas maupun Vinyl.</ListGroup.Item>
		      	<ListGroup.Item>Dll.</ListGroup.Item>
		      </ListGroup>
		     
		    </Card>
		</div>
		<br />
		<section className="Footer">
			 		<table>
			 			<tr>
			 				<td width="10%"></td>
			 				<td className="color">

			 					Alamat Kami : 
			 					<br />
								Jl. Pandigiling Surabaya 
								<br />

								 +123 7854 1263 <br />

								 info@example.com <br/>


			 				</td>
			 			</tr>
			 		</table>
			 	</section>
	 </>
	)
}

export default Pelayanan;