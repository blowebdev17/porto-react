import React, { useState, useEffect } from "react";
import { useNavigate } from 'react-router-dom';
import Header from './Header';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';

function Kontak() {	
	return (
	 <>
		<Header />
		<br />
		<div className="container-fluid">
			<Card className="textCard">
		      <Card.Body>
		        <Card.Title>Kontak Kami</Card.Title>
		        <Card.Text>
		          Kontak kami di bawah ini.
		        </Card.Text>
		      </Card.Body>
		      <ListGroup className="list-group-flush">
		        <ListGroup.Item>
		        	OPEN: 9.30 AM - 17.30 PM (Minggu & Hari Libur : Tutup) <br/>
					Jl. Pandigiling, no. 10 Surabaya Indonesia <br />

Admin 1 : +62-361-4712640 <br />
Admin 2 :+62-857-38-224455 <br />
Admin 3 : +62-857-38-224455 <br />
email : cs@onemedia.net <br />
		        </ListGroup.Item>
		       
		      </ListGroup>
		     
		    </Card>
		</div>
	 </>
	)
}

export default Kontak;