import React, { useState, useEffect } from "react";
import { useNavigate } from 'react-router-dom';
import Header from './Header'
import Alert from 'react-bootstrap/Alert';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';

function Home() {	
	return (
	 <>
	 <div>
		<Header />

		 <div className="wrapper">

		 	<section className='Home' >
		 		<table className="section_home">
		 		<tr>
		 			<td width="20%"></td>
		 			<td width="80%" className="title">
		 				<h1 className="textBold">Selamat datang</h1>
		 				<h4>Di website one media printing kami menyediakan segala kebutuhan anda, mulai percetakan banner, flyer dan lain lain.</h4>
		 			</td>
		 			<td>
		 				<img src="gambar.webp" />
		 			</td>
		 		</tr>
		 		</table>
		 	</section>

		 	<section  className="section_produk_layanan">
		 		<br />
		 		<h3>Kenapa pilih kami ?</h3>
		 		<h6>One media printing sudah berpengalaman puluhan tahun mengembangkan bisnis percetakan	</h6>
		 		<br />
		 		<table width="100%">
		 			<td width="10%"></td>
		 			<td width="10%">
		 				 
		 				 <Card className="cardBody" border="primary">
					      <Card.Body>
					        <Card.Title>
					        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAACXBIWXMAAAsTAAALEwEAmpwYAAADq0lEQVR4nO2ayWtUQRDGf8HojChqxiUe9SgR9Z9wicYtN7ebEi8uRK8uN8XoRQjk71A0qKgE1KhxFzSLgrgdNHgzGqKMFHwPCp2ZvHnT8+YpfvCYpbur++vqrq6qfvAf/y4KwDagB7gEDANfgEk99v2lyqzOVqCFjCAP7AGuAj+BYpXPD+AKsBvINYLAbOAI8NEN6jtwAzgmzazQjM/U06L/rOw4cFNtovYfgG5NTirYBLx2AxgC9gLzE8haAOwDHjh5r4B26gibqT7X4UNgXUD5G4DHTn5vPbSzVAO3Dr4CB4AZMdveAwZj1p0BHAImnLZbCYTlUndRVmdlle2jGa4Gq4ARtRvTGGrCYifwPrAogYwkRJBxuKW2r7UqEiHvltMdYE5COUmJoD7vumWWaM/0ueVkhx0NIGJY6FaFGYCqTWy0savdE6GJRHsmMgBm3WIfdtE5YdaJDBAxHHabP9YSO+rOibgmNg0izcATyTJSFZGTq2CV1xIGoYgYNkrWx+m0sttZCDJIpMlZ0p2VKl5TJfOdskjE0CV5/ZRBQW7194QOYFpEWhTfTJUb53Z1eJ2wCE3EMCCZmymBsyq0eCLrRE5K5plShZdUuOUvILJdMi+UKhxVoUVxIWBR4TlHpEf/hUCbZFpe4A+Mq9B8mxDwJDyZEFgkeZ9LFU6qcFagzj6XIPIpkOycyxPUncjbEkTep0FkPODSsoDsTQkip0hhaY0G2uxLgOeS9UaaMf/tdEBtt1Xa7JH5tQxgLZp45gKyxOHpNOisZH6jA9GSZ7Vq4rl+1wsnKx2I21Ro2cKsauJ3F6WDMs5Y5DRaBjCLmoic28hpnEcZXNGALI0ZF0MpkjDsV3+XqYBdqmS52LgYVP00SDQBjzTGHdMdNO9VcT3ZQ4fG9i7ONUS3Kj8KlHwIhWbgqcZ2ME6DvMv1WkI5K+jWmEaquRRqV6MJJccajTXAN42p6quMXjcDoVx7Ep5RYxrL+SQC8s603q0hiV0L5up+pajPXC2zMeyuFex3WigAt911XM0XPsudam2ZrSadPTGmPs0rXxZKcKtbZhPKvZo5DI1mWadvbjkFP2jzzgAUlVBuD3hi21VGdE5EG7uud+8bnNqjrH1XwrcYCvKdIrejqKUU8rZ4Wu0cdu5MUR6pudcnlHdqk9mepWehLo06VWfA5Qkit+Ngo96AyCkr3q8QoFjlM6XIdEejCJTCfOViLWq7qABr3L1UY99fKDw9o7pl44n/4C/HL07KXY9ffv2xAAAAAElFTkSuQmCC" />
		 					<br />
		 					<br />

					        Tepat waktu</Card.Title>
					        <Card.Text>
					        Kami melayani tepat waktu
					        </Card.Text>
					      </Card.Body>
					       <Card.Body>

					      </Card.Body>
					    </Card>

		 			</td>
		 			<td width="10%">

		 					<Card className="cardBody" border="primary">
						      <Card.Body>
						        <Card.Title>
						        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAACXBIWXMAAAsTAAALEwEAmpwYAAADq0lEQVR4nO2ayWtUQRDGf8HojChqxiUe9SgR9Z9wicYtN7ebEi8uRK8uN8XoRQjk71A0qKgE1KhxFzSLgrgdNHgzGqKMFHwPCp2ZvHnT8+YpfvCYpbur++vqrq6qfvAf/y4KwDagB7gEDANfgEk99v2lyqzOVqCFjCAP7AGuAj+BYpXPD+AKsBvINYLAbOAI8NEN6jtwAzgmzazQjM/U06L/rOw4cFNtovYfgG5NTirYBLx2AxgC9gLzE8haAOwDHjh5r4B26gibqT7X4UNgXUD5G4DHTn5vPbSzVAO3Dr4CB4AZMdveAwZj1p0BHAImnLZbCYTlUndRVmdlle2jGa4Gq4ARtRvTGGrCYifwPrAogYwkRJBxuKW2r7UqEiHvltMdYE5COUmJoD7vumWWaM/0ueVkhx0NIGJY6FaFGYCqTWy0savdE6GJRHsmMgBm3WIfdtE5YdaJDBAxHHabP9YSO+rOibgmNg0izcATyTJSFZGTq2CV1xIGoYgYNkrWx+m0sttZCDJIpMlZ0p2VKl5TJfOdskjE0CV5/ZRBQW7194QOYFpEWhTfTJUb53Z1eJ2wCE3EMCCZmymBsyq0eCLrRE5K5plShZdUuOUvILJdMi+UKhxVoUVxIWBR4TlHpEf/hUCbZFpe4A+Mq9B8mxDwJDyZEFgkeZ9LFU6qcFagzj6XIPIpkOycyxPUncjbEkTep0FkPODSsoDsTQkip0hhaY0G2uxLgOeS9UaaMf/tdEBtt1Xa7JH5tQxgLZp45gKyxOHpNOisZH6jA9GSZ7Vq4rl+1wsnKx2I21Ro2cKsauJ3F6WDMs5Y5DRaBjCLmoic28hpnEcZXNGALI0ZF0MpkjDsV3+XqYBdqmS52LgYVP00SDQBjzTGHdMdNO9VcT3ZQ4fG9i7ONUS3Kj8KlHwIhWbgqcZ2ME6DvMv1WkI5K+jWmEaquRRqV6MJJccajTXAN42p6quMXjcDoVx7Ep5RYxrL+SQC8s603q0hiV0L5up+pajPXC2zMeyuFex3WigAt911XM0XPsudam2ZrSadPTGmPs0rXxZKcKtbZhPKvZo5DI1mWadvbjkFP2jzzgAUlVBuD3hi21VGdE5EG7uud+8bnNqjrH1XwrcYCvKdIrejqKUU8rZ4Wu0cdu5MUR6pudcnlHdqk9mepWehLo06VWfA5Qkit+Ngo96AyCkr3q8QoFjlM6XIdEejCJTCfOViLWq7qABr3L1UY99fKDw9o7pl44n/4C/HL07KXY9ffv2xAAAAAElFTkSuQmCC" />
		 					<br />
		 					<br />
						        Fast Respons</Card.Title>
						        <Card.Text>
						        Kami melayani tepat waktu
						        </Card.Text>
						      </Card.Body>
						       <Card.Body>

						      </Card.Body>
						    </Card>

		 			</td>
		 			<td width="10%">
		 				<Card className="cardBody" border="primary">
					      <Card.Body>
					        <Card.Title>
					        	<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAACXBIWXMAAAsTAAALEwEAmpwYAAADq0lEQVR4nO2ayWtUQRDGf8HojChqxiUe9SgR9Z9wicYtN7ebEi8uRK8uN8XoRQjk71A0qKgE1KhxFzSLgrgdNHgzGqKMFHwPCp2ZvHnT8+YpfvCYpbur++vqrq6qfvAf/y4KwDagB7gEDANfgEk99v2lyqzOVqCFjCAP7AGuAj+BYpXPD+AKsBvINYLAbOAI8NEN6jtwAzgmzazQjM/U06L/rOw4cFNtovYfgG5NTirYBLx2AxgC9gLzE8haAOwDHjh5r4B26gibqT7X4UNgXUD5G4DHTn5vPbSzVAO3Dr4CB4AZMdveAwZj1p0BHAImnLZbCYTlUndRVmdlle2jGa4Gq4ARtRvTGGrCYifwPrAogYwkRJBxuKW2r7UqEiHvltMdYE5COUmJoD7vumWWaM/0ueVkhx0NIGJY6FaFGYCqTWy0savdE6GJRHsmMgBm3WIfdtE5YdaJDBAxHHabP9YSO+rOibgmNg0izcATyTJSFZGTq2CV1xIGoYgYNkrWx+m0sttZCDJIpMlZ0p2VKl5TJfOdskjE0CV5/ZRBQW7194QOYFpEWhTfTJUb53Z1eJ2wCE3EMCCZmymBsyq0eCLrRE5K5plShZdUuOUvILJdMi+UKhxVoUVxIWBR4TlHpEf/hUCbZFpe4A+Mq9B8mxDwJDyZEFgkeZ9LFU6qcFagzj6XIPIpkOycyxPUncjbEkTep0FkPODSsoDsTQkip0hhaY0G2uxLgOeS9UaaMf/tdEBtt1Xa7JH5tQxgLZp45gKyxOHpNOisZH6jA9GSZ7Vq4rl+1wsnKx2I21Ro2cKsauJ3F6WDMs5Y5DRaBjCLmoic28hpnEcZXNGALI0ZF0MpkjDsV3+XqYBdqmS52LgYVP00SDQBjzTGHdMdNO9VcT3ZQ4fG9i7ONUS3Kj8KlHwIhWbgqcZ2ME6DvMv1WkI5K+jWmEaquRRqV6MJJccajTXAN42p6quMXjcDoVx7Ep5RYxrL+SQC8s603q0hiV0L5up+pajPXC2zMeyuFex3WigAt911XM0XPsudam2ZrSadPTGmPs0rXxZKcKtbZhPKvZo5DI1mWadvbjkFP2jzzgAUlVBuD3hi21VGdE5EG7uud+8bnNqjrH1XwrcYCvKdIrejqKUU8rZ4Wu0cdu5MUR6pudcnlHdqk9mepWehLo06VWfA5Qkit+Ngo96AyCkr3q8QoFjlM6XIdEejCJTCfOViLWq7qABr3L1UY99fKDw9o7pl44n/4C/HL07KXY9ffv2xAAAAAElFTkSuQmCC" />
		 					<br />
		 					<br />
					        Terbaik</Card.Title>
					        <Card.Text>
					        Kami melayani tepat waktu
					        </Card.Text>
					      </Card.Body>
					       <Card.Body>

					      </Card.Body>
					    </Card>
		 			</td>
		 			<td width="10%">
		 				<Card className="cardBody" border="primary">
					      <Card.Body>
					        <Card.Title>
					        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAACXBIWXMAAAsTAAALEwEAmpwYAAAFOklEQVR4nO2aW4iVVRTHf05n5qSlzZhOF3sQn7yVFeGDEEGQYlpOd9K3iC7Y1GhFVGSRUl4yiER8iEkqguqlNC17KOqhoKRE1LFJIbLLRNlDF5wzjjOx6P/B4us753x7f+dMEf1hwzmz9l57r73X2uu/1xn4H/9ddABdwCZgF3AY+AWoqNnnPsmszzKgnX8JysAKYA8wDIwGNhvzDrBcusYc44HVwHduUYPAB8AancwsnVIr0KbPsyVbo76Dbvy3QA9w+lgZsRg44hbwOXBHpJuYcXdKR6LvK2ARTYTt1FY34T4ZNa4Buk3HEmC/07+lGe52DrBXE5wAVgGlRk/CXzofcC73KdDZKOXTddzJsV9M83GJc99+raEQpuoaNYWfNXJ3cmAy8InmNqPOLRITe90RT4zUk/h8zPiJqTVExcxW5052MvwDhiAvOCodzxOIxS6wY2OiRb6dGDIXOC1S16XuAlgYkuySQLOkF4orgDe1CelMbn97FbgwQu+DLvhzudhqlydCr9j1wIjGf6zvq9Secy5iu3tboO5Wl2fuq9e57GiHuVcI7nZUY0GNxGeLOKV2deAcSzXHsXqnssLRjpCMPUHsdlg5oB4e0zxfa6fzYpw8xcbeWqvjHnUy7hSCazXuvYA4PK4xRudjTn53LQI3LP9tjwzEZwPGvKYxmyMSZQU4CUzK6tAlxe8TjnUa+2jAmEfkjtsi5vtI812TJXxGQnsrhOIejX2RscETmm9jlvDtSJ9FDyYb+zswg+bjOs23M0vYL6G97GKw091EV9FczNFcRmj/hp8lPLsAJ/rCZXErNDwEzBNdaSSmaI6fsoQVCe19HYvx8t/kak2abdIbwO3ABRRH2TGEphjiDVqi52riskk7pcrJZQ0w5EQzXKsWzgNuVu5IyKTlrHsj9U2Vjh+yhF8WDPa86FASHFHLTcszgt3isOr1a4lxLPC05vswYuwNGrsjS7gpMiGaK74OvBw47izRjKGIJ+wCYABYmyVcJkOsAhiCNgXwcCCTNXyjTUgMKQHXA73AQcXtcX3erpMoOc41M0tpuyONHRELsk2YH0jJn3LPBeNNScWmVuvP4/7vqrOVMUOwTeNeCDQkaevcy/IwcD9wkTa0Q5/t5XpIfUb0+qyaaJdHPqzmS/nJiLrtWs1psdJT53ltxYtul/M2VOtY1lN1VAktpnz0h/JDtXg5H5jm3GlERmRtwFu6TdO40hljBDITPeqwP7D4UJJrjbpk1Ss3McOeVEb/XhtWcjFRrZgwJHkWVrq6W2u1CmNS67WCckw5yG6iHzMC1Wj+wykq3lej3jVUwxAbc0Dym6otZpEjZVYci8U0Z8SM1An35qidDdUwBJ20yV+qtYgtroBcpHidGJLGQf3dbqRYQ2ZLbvSqKsoqHCeV+KJF7DQSqp/kLKtOjuZsSQXlTH3/td4iOh0Nb/TPCmlDEq6Xp1nZynCGvv+WZ8LpzpgjBWOm0a41y2X7XOh0bjaoOlYor0pjewOCvVvyV0ImLuv3iVGXZ5YW+DE0oeKHIq/fFlfUviVmAQtTz9d9KmMaGw1Byemxnc1CLUPukuxoEe8oKxsfcwZVVAF8XFR8rt4qbeo/RX+70VXru0RRKqIdaex2ge1xufvhx/QVRllV8V0ijHlvnQF3guvdRqysUzpq0UkMRtaNc2GSSOBGFev69DCqqOAwIG61Q7R9plvcBmfkAWXsObpeJyjxdaf+oWBzgZ/ymgrjXgnHq9UsJhriTs2EBa0RQONOdnpGMq0ZBbEr1m6nzMD+E6Ag8BEP534sAAAAAElFTkSuQmCC" />
					        <br />
		 					<br />
					        Murah</Card.Title>
					        <Card.Text>
					        Harga kami bersaing
					        </Card.Text>
					      </Card.Body>
					       <Card.Body>

					      </Card.Body>
					    </Card>
		 			</td>
		 			<td width="10%"></td>
		 		</table>
		 		<br />
		 		<br />
		 	</section>

		 	<section  className="">
		 		<br />
		 		<h3>Testimonial</h3>
		 		<h6>Testimonial One media printing</h6>
		 		<br />
		 		<table width="100%">
		 			<td width="10%"></td>
		 			<td width="3%">
		 				 
		 				 <Card className="cardBody">
					      <Card.Body>
					        <Card.Title>
					        Sapri</Card.Title>
					        <Card.Text>
					        <i>"Kualitasnya bagus"</i>
					        </Card.Text>
					      </Card.Body>
					       <Card.Body>

					      </Card.Body>
					    </Card>
		 			</td>
		 			<td width="3%">
		 				 
		 				 <Card className="cardBody">
					      <Card.Body>
					        <Card.Title>
					        Ahmad</Card.Title>
					        <Card.Text>
					        <i>"Kualitasnya bagus"</i>
					        </Card.Text>
					      </Card.Body>
					       <Card.Body>

					      </Card.Body>
					    </Card>

		 			</td>

		 			<td width="3%">
		 				 
		 				 <Card className="cardBody" >
					      <Card.Body>
					        <Card.Title>
					        Cahyo</Card.Title>
					        <Card.Text>
					        <i>"Sangat Puas"</i>
					        </Card.Text>
					      </Card.Body>
					       <Card.Body>

					      </Card.Body>
					    </Card>

		 			</td>
		 		
		 			<td width="10%"></td>
		 		</table>
		 		<br />
		 		<br />
		 	</section>
			 	<section className="Footer">
			 		<table>
			 			<tr>
			 				<td width="10%"></td>
			 				<td className="color">

			 					Alamat Kami : 
			 					<br />
								Jl. Pandigiling Surabaya 
								<br />

								 +123 7854 1263 <br />

								 info@example.com <br/>


			 				</td>
			 			</tr>
			 		</table>
			 	</section>
			</div>
		</div>
	 </>
	)
}

export default Home;