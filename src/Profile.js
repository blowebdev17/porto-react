import React, { useState, useEffect } from "react";
import { useNavigate } from 'react-router-dom';
import Header from './Header'
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';

function Profile() {	
	return (
	 <>
		 <div>
			<Header />
			<br />

			<section className='Home' >
		 		<table className="section_home">
		 		<tr>
		 			<td width="5%"></td>
		 			<td width="80%" className="textProfile">
		 				<h3>Tentang Kami</h3>
		 				<br />
		 				One Media adalah home industri dan salah satu percetakan di Surabaya untuk memberikan pelayanan jasa cetak yang berada di Surabaya maupun di luar wilayah Surabaya.

								Kami telah berpengalaman sejak tahun 2021, dalam hal pengadaan barang cetakan untuk keperluan kantor, organisasi, instansi pemerintahan dan menjadi supplier cetak dalam satu tempat untuk berbagai keperluan guna menunjang sarana prasana media promosi indoor-outdoor seperti untuk acara pertemuan, event, forum, conference, pameran dan sebaginya.

								Dengan dukungan prasarana mesin cetak Offset dan Digital printing yang memadai untuk hasil yang berkwalitas, One Media terus berupaya mengadakan peningkatan kwalitas produk sesuai dengan perkembangan teknologi kususnya dalam pencetakan guna dapat memberikan hasil yang lebih optimal bagi setiap konsumen.

								Harga yang kompetitif merupakan suatu hal yang terpenting bagi kami untuk setiap pelanggan. Tentunya diimbangi dengan kwalitas akan mutu cetakan untuk setiap produk yang dikerjakan di One Media.

								<br />
								<br />

								PROSES ORDER
								<br />

								Untuk mendapatkan hasil cetakan yang maksimal dan sesuai harapan, pada website ini kami juga memberikan acuan dasar sebagai pertimbangan dalam melakukan pemesanan cetakan di One Media Surabaya.

								<br />
								<br />
								PRODUK DAN LAYANAN
								<br />
								One Media menyediakan semua format untuk pencetakan konvensional, dengan menggunakan Digital Printer, Mesin Cetak Offset Separasi, dan reproduksi warna digital yang memadai untuk mendapatkan kualitas yang lebih baik dan sempurna.

								Pencetakan untuk keperluan Kantor (ATK), berbagai jenis Buku, Booklet, Brosur, Flyer, aneka jenis Kartu, Signboard, Banner, Sticker dan sebagainya dengan berbagai ukuran dan jenis bahan kertas maupun Vinyl.

								Untuk memudahkan konsumen dalam melakukan usaha promosi, One Media melayani pengurusan perijinan advertising / reklame seperti Billboard, Baliho, spanduk dan umbul-umbul.


								<br />
								<br />
								JAMINAN PRODUK
								<br />
								Percetakan One Media Surabaya memberikan jaminan untuk setiap pesanan, akan sesuai dengan permintaan konsumen di dalam pengerjaannya
								<br />
								Untuk itu kami memberikan jaminan kepuasan
								konsumen diantaranya : <br />
								- Jaminan terhadap hasil produksi,
								apabila mengalami cacat produksi, atau kerusakan oleh karena mesin/ peralatan kami pada saat pencetakan.
								Untuk produk tertentu, kami memberikan jaminan berupa penggantian barang yang sama / sejenis, apabila kwalitas produk kami tidak sesuai dengan permintaan saat perjanjian order dilakukan.

								<br />
								<br />
								Jaminan Tepat Waktu
								Untuk order yang sifatnya mendesak /urgent, seperti untuk keperluan acara, event, conference, forum dan acara yang akan
								diselenggarakan dalam waktu yang relatif singkat, kami di percetakan One Media siap membantu anda untuk menyelesaikan jenis order yang akan anda berikan akan selesai dengan tepat waktu sesuai dengan perjanjian yang telah disepakati.
		 				<br />
		 				<br />
		 			</td>
		 			<td className="fotoBg">
		 				<img src="bg_profile.png"  className="Ft" />
		 				<br />
		 				<img src="bg_profile 2.png"  className="Ft" />
		 			</td>
		 			<td width="10%"></td>
		 		</tr>
		 		</table>
		 	</section>
			<section className="Footer">
			 		<table>
			 			<tr>
			 				<td width="10%"></td>
			 				<td className="color">

			 					Alamat Kami : 
			 					<br />
								Jl. Pandigiling Surabaya 
								<br />

								 +123 7854 1263 <br />

								 info@example.com <br/>


			 				</td>
			 			</tr>
			 		</table>
			 	</section>
		</div>
	 </>
	)
}

export default Profile;