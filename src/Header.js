
import { Navbar, NavDropdown } from 'react-bootstrap';
import { Container } from 'react-bootstrap';
import { Nav } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import "./Header.css"

function Header() {
	return (
		<div>
			<Navbar variant="dark" className="custom_nav">
			    <Container>
			    <Navbar.Brand href="/" style={{ color: 'black' }} className="Logo">One Media</Navbar.Brand>
			    <Nav className="me-auto navbar_wrapper">
		      		 <Link className="navbar_link" to="/">Home</Link>
		      		 <Link className="navbar_link" to="/profile">Profile</Link>
		      		 <Link className="navbar_link" to="/pelayanan">Pelayanan</Link>
		      		 <Link className="navbar_link" to="/Kontak">Kontak</Link>
			    </Nav>
		    </Container>
		  </Navbar>
		</div>
	)
}

export default Header;