import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Button } from 'react-bootstrap';
import Header from './Header.js';
import { BrowserRouter, Routes, Route, Link, Switch } from 'react-router-dom';
import Home from './Home';
import Profile from './Profile';
import Pelayanan from './Pelayanan';
import Kontak from './Kontak';

function App() {
  return (
   <div className="App">
     <BrowserRouter>
        <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/profile" element={<Profile />} />
            <Route path="/pelayanan" element={<Pelayanan />} />
            <Route path="/kontak" element={<Kontak />} />
        </Routes>
     </BrowserRouter>
   </div>
  );
}

export default App;
